from django.urls import path
from .views import *


app_name="ecomapp"
urlpatterns=[
	path("",ClientHomeView.as_view(),name='clienthome'),
	path("add-to-cart/i-<i_id>/",ClientAddToCartView.as_view(),name='clientaddtocart'),
	path("cart/",ClientCartView.as_view(),name='clientcart'),
	path("manage-cart/<ci_id>-<action>/",ClientManageCartView.as_view(),name='clientmanagecart'),
	path("signup/",CustomerSignupView.as_view(),name='customersignup'),
	path("signin/",CustomerSigninView.as_view(),name='customersignin'),
	path("signout/",CustomerSignoutView.as_view(),name='customersignout'),
	path("checkout/",CustomerCheckoutView.as_view(),name='customercheckout'),
	path("emptycart/",CustomerEmptyCartView.as_view(),name='customeremptycart'),





	#admin site urls
	path("admin-home/",AdminHomeView.as_view(),name='adminhome'),
	path("admin-login/",AdminLoginView.as_view(),name='adminlogin'),
	path("admin-logout/",AdminLogoutView.as_view(),name='adminlogout'),
	path("my-admin/order-<int:pk>/detail/",AdminOrderDetailView.as_view(),name='adminorderdetail'),
	path("my-admin/order-<order_id>/make-payment/",AdminMakePaymentView.as_view(),name='adminmakepayment'),
	path("my-admin/order-<order_id>/change-status/",AdminMakeStatusView.as_view(),name='adminchangestatus'),
	path("my-admin/item-create/",AdminItemCreateView.as_view(),name='adminitemcreate'),
	path("my-admin/item/<int:pk>/update/",AdminItemUpdateView.as_view(),name='adminitemupdate'),
	path("my-admin/item-list/",AdminItemListView.as_view(),name='adminitemlist'),




	]