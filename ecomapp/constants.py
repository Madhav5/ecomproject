DISTRICTS = (
    ("Palpa", "Palpa"),
    ("Jhapa", "Jhapa"),
    ("Kathmandu", "Kathmandu"),
)


STATUS = (
    ("Pending", "Pending"),
    ("Processing", "Processing"),
    ("Delivered", "Delivered"),
    ("Canceled", "Canceled"),
)
