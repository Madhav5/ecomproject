from django.contrib import admin

# Register your models here.

from .models import *

# Register your models here.
admin.site.register(Admin)
admin.site.register(Customer)
admin.site.register(Item)
admin.site.register(Order)
admin.site.register(CartItem)
admin.site.register(Cart)
admin.site.register(ItemImage)
admin.site.register(Category)


