from django.shortcuts import render, redirect
from django.views.generic import *
from .models import *
from .forms import *
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse, reverse_lazy
from django.http import JsonResponse


# Create your views here.
class ClientMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['root_categories'] = Category.objects.filter(root=None)

        return context


class ClientHomeView(ClientMixin, TemplateView):
    template_name = "clienttemplates/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.GET:
            cat_slug = self.request.GET.get("cat")
            cat_obj = Category.objects.get(slug=cat_slug)
            all_items = Item.objects.filter(category=cat_obj)
            context['all_items'] = all_items

        else:
            context['all_items'] = Item.objects.all()

        return context


class ClientAddToCartView(View):
    def get(self, request, *args, **kwargs):
        item_id = self.kwargs['i_id']
        item = Item.objects.get(id=item_id)
        cart_id = self.request.session.get("cart_id")
        if cart_id:
            cart = Cart.objects.get(id=cart_id)
        else:
            cart = Cart.objects.create(total=0)
            self.request.session["cart_id"] = cart.id
        try:
            ci = cart.cartitem_set.get(item=item)
            ci.quantity += 1
            ci.subtotal += ci.rate
            ci.save()
            cart.total += ci.rate
            cart.save()

        except:
            ci = CartItem.objects.create(
                cart=cart, item=item, rate=item.sp, quantity=1, subtotal=item.sp)
            cart.total += ci.rate
            cart.save()

        return render(request, "clienttemplates/addtocart.html",
                      {})


class ClientCartView(ClientMixin, TemplateView):
    template_name = "clienttemplates/clientcart.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cart_id = self.request.session.get("cart_id")
        if cart_id:
            cart = Cart.objects.get(id=cart_id)
        else:
            cart = None
        context["cart"] = cart

        return context


class ClientManageCartView(View):
    def get(self, request, *args, **kwargs):
        ci_id = self.kwargs['ci_id']
        action = self.kwargs['action']
        ci = CartItem.objects.get(id=ci_id)
        cart = ci.cart
        if action == 'inc':
            ci.quantity += 1
            ci.subtotal += ci.rate
            ci.save()
            cart.total += ci.rate
            cart.save()

        elif action == "dcr":
            ci.quantity -= 1
            ci.subtotal -= ci.rate
            ci.save()
            cart.total -= ci.rate
            cart.save()

            if ci.quantity == 0:
                ci.delete()

        elif action == 'rmv':
            cart.total -= ci.subtotal
            cart.save()
            ci.delete()

        else:
            print("Invalid operation")

        return redirect('/cart/')


class CustomerEmptyCartView(View):
    def get(self, request, *args, **kwargs):
        c_id = request.session.get('cart_id')
        if c_id:
            cart = Cart.objects.get(id=c_id)
            cartitem = cart.cartitem_set.all()
            cartitem.delete()
            cart.total = 0
            cart.save()
        else:
            return redirect('ecomapp:clienthome')

        return redirect("/cart/")


class CustomerSignupView(ClientMixin, CreateView):
    template_name = 'clienttemplates/customersignup.html'
    form_class = CustomerForm
    success_url = reverse_lazy('ecomapp:clienthome')

    def form_valid(self, form):
        email = form.cleaned_data['email']
        password = form.cleaned_data['password']
        user = User.objects.create_user(email, email, password)
        form.instance.user = user
        login(self.request, user)

        return super().form_valid(form)


class CustomerSigninView(ClientMixin, FormView):
    template_name = 'clienttemplates/customersignin.html'
    form_class = LoginForm
    success_url = reverse_lazy('ecomapp:clienthome')

    def form_valid(self, form):
        username = form.cleaned_data['email']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            try:
                customer = user.customer
                login(self.request, user)
            except:
                return render(self.request, self.template_name, {
                    'form': form,
                    'error': "Invalid Credential"
                })
        else:
            return render(self.request, self.template_name, {
                'form': form,
                'error': "Invalid Credential"
            })

        return super().form_valid(form)

    def get_success_url(self):
        if self.request.GET:
            next_url = self.request.GET.get('next')
            return next_url
        else:
            return self.success_url


class CustomerSignoutView(View):
    def get(self, request):
        logout(request)
        return redirect('/')


class CustomerCheckoutView(ClientMixin, CreateView):
    template_name = "clienttemplates/checkout.html"
    form_class = OrderForm
    success_url = reverse_lazy("ecomapp:clienthome")

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            try:
                customer = request.user.customer
                if "cart_id" in request.session:
                    cart_id = request.session["cart_id"]
                    cart = Cart.objects.get(id=cart_id)
                    cart.customer = customer
                    cart.save()
                    self.thiscart = cart
                    if cart.cartitem_set.count() < 1:
                        return redirect("ecomapp:clienthome")
                else:
                    return redirect("ecomapp:clienthome")

            except:
                return redirect("/signin/?next=/checkout/")

        else:
            return redirect("/signin/?next=/checkout/")

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cart'] = self.thiscart
        return context

    def form_valid(self, form):
        c_id = self.request.session.get('cart_id')
        if c_id:
            cart = Cart.objects.get(id=c_id)
            if cart.cartitem_set.count() > 0:
                # create order
                form.instance.cart = cart
                form.instance.net_total = cart.total
                form.instance.status = "Pending"
                #for decreasing stock 
                for ci in cart.cartitem_set.all():
                    ci.item.stock-=ci.quantity
                    ci.item.save()
                del self.request.session['cart_id']

            else:
                return redirect("ecomapp:clienthome")

        else:
            return redirect("ecomapp:clienthome")

        return super().form_valid(form)


#//admin side view //#

class AdminRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if request.user.is_authenticated:
            try:
                admin = request.user.admin

            except:
                return redirect("/admin-login/")

        else:
            return redirect("/admin-login")

        return super().dispatch(request, *args, **kwargs)


class AdminHomeView(AdminRequiredMixin, TemplateView):
    template_name = "admintemplates/adminhome.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['allorders'] = Order.objects.all().order_by('-id')

        return context


class AdminLoginView(FormView):
    template_name = "admintemplates/adminlogin.html"
    form_class = LoginForm
    success_url = "/admin-home/"

    def form_valid(self, form):
        x = form.cleaned_data["email"]
        y = form.cleaned_data["password"]
        usr = authenticate(username=x, password=y)
        if usr is not None:
            login(self.request, usr)

        else:
            return render(self.request, 'admintemplates/adminlogin.html',
                          {
                              "error": "Invalid Credential",
                              "form": form
                          })

        return super().form_valid(form)


class AdminLogoutView(View):
    def get(self, request):
        logout(request)

        return redirect('/admin-home/')


class AdminOrderDetailView(AdminRequiredMixin, DetailView):
    template_name = "admintemplates/adminorderdetail.html"
    queryset = Order.objects.all()
    context_object_name = "order"


class AdminMakePaymentView(AdminRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        o_id = self.kwargs['order_id']
        try:
            order = Order.objects.get(id=o_id)
            order.payment_status = True
            order.save()
            message = 'payment success'
        except:
            message = 'payment unsuccessful'

        return JsonResponse({'message': message})


class AdminMakeStatusView(AdminRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        o_id = self.kwargs['order_id']
        try:
            order = Order.objects.get(id=o_id)
            action = request.GET.get('act')
            if action == 'p':
                order.status = "Processing"
            elif action == 'd':
                order.status = "Delivered"

            elif action == "c":
                order.status = "Canceled"
            else:
                message = "Invalid Operation"

            order.save()
            message = order.status

        except:
            message = 'Invalid'
        return JsonResponse({'message': message})


class AdminItemCreateView(CreateView):
    template_name = "admintemplates/adminitemcreate.html"
    form_class = ItemAddForm
    success_url = "/admin-home/"

    def form_valid(self, form):
        images=self.request.FILES.getlist('images')
        item=form.save()
        for i in images:
            ItemImage.objects.create(item=item,image=i)

        return super().form_valid(form)


class AdminItemUpdateView(UpdateView):
    template_name = "admintemplates/adminitemcreate.html"
    form_class = ItemAddForm
    success_url = "/admin-home/"
    model = Item


class AdminItemListView(ListView):
    template_name = "admintemplates/adminitemlist.html"
    queryset = Item.objects.all().order_by("-id")
    context_object_name = "allitems"
