from django.db import models

from django.contrib.auth.models import User
from .constants import *


class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True, null=True, blank=True)

    class Meta:
        abstract = True


class Admin(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='admins')
    address = models.CharField(max_length=400, null=True, blank=True)
    mobile = models.CharField(max_length=50)

    def __str__(self):
        return self.full_name


class Customer(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='customers')
    address = models.CharField(max_length=400, null=True, blank=True)
    mobile = models.CharField(max_length=50)

    def __str__(self):
        return self.full_name


class Category(TimeStamp):
    title = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)
    image = models.ImageField(upload_to='categories')
    root = models.ForeignKey(
        "self", on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.title


class Item(TimeStamp):
    category=models.ForeignKey(Category,on_delete=models.SET_NULL,null=True,blank=True)
    title = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)
    mrp = models.PositiveIntegerField()
    sp = models.PositiveIntegerField()
    description = models.TextField()
    view_count = models.PositiveIntegerField()
    sale_count = models.PositiveIntegerField()
    stock = models.PositiveIntegerField()

    def __str__(self):
        return self.title


class ItemImage(TimeStamp):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='items')

    def __str__(self):
        return self.item.title


class Cart(TimeStamp):
    customer = models.ForeignKey(
        Customer, on_delete=models.SET_NULL, null=True, blank=True)
    total = models.PositiveIntegerField(default=0)

    def __str__(self):
        return "cart_" + str(self.id)


class CartItem(TimeStamp):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.SET_NULL, null=True, blank=True)
    rate = models.PositiveIntegerField()
    quantity = models.PositiveIntegerField()
    subtotal = models.PositiveIntegerField()

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.item.title


class Order(TimeStamp):
    cart = models.OneToOneField(Cart, on_delete=models.CASCADE)
    discount = models.PositiveIntegerField(default=0)
    net_total = models.PositiveIntegerField(default=0)
    name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=10)
    email = models.EmailField()
    district = models.CharField(max_length=50, choices=DISTRICTS)
    street_address = models.CharField(max_length=100)
    order_note = models.CharField(max_length=50, null=True, blank=True)
    status = models.CharField(max_length=50, choices=STATUS)
    payment_status = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return "order_" + str(self.id)
