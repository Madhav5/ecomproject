from django import forms
from .models import *
from django.contrib.auth.models import User


class LoginForm(forms.Form):
    email = forms.CharField(widget=forms.EmailInput(attrs={
        "class": "form-control",
        "autocomplete": "off",
        "placeholder": "Enter Email"
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': "form-control",
        "autocomplete": "off",
        "placeholder": "Enter password"
    }))


class CustomerForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.EmailInput())
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = Customer
        fields = ['email', 'password', 'full_name',
                  'image', 'address', 'mobile']


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['name', 'mobile', 'email', 'district', 'street_address']


class ItemAddForm(forms.ModelForm):
    images=forms.FileField(widget=forms.FileInput(attrs={
    	'class':'form-control',
        'multiple':'true'
    	}))
    # category = forms.ModelChoiceField(
    #     queryset=Category.objects.exclude(root=None), widget=forms.Select(attrs={
    #             'class': 'form-control'
    #             }))

    class Meta:
        model = Item
        fields = ['category', 'title', 'slug', 'mrp', 'sp',
                  'description', 'view_count', 'sale_count', 'stock']
        widgets = {
            'category': forms.Select(attrs={
                'class': 'form-control'
            }),

            'title': forms.TextInput(attrs={
                "class": "form-control",
            }),

            'slug': forms.TextInput(attrs={
                "class": "form-control",
            }),

            'mrp': forms.NumberInput(attrs={
                "class": "form-control",
            }),

            'sp': forms.NumberInput(attrs={
                "class": "form-control",
            }),

            'description': forms.Textarea(attrs={
                "class": "form-control",
                "row": 5,
            }),

            'view_count': forms.NumberInput(attrs={
                "class": "form-control",
            }),

            'sale_count': forms.NumberInput(attrs={
                "class": "form-control",
            }),

            'stock': forms.NumberInput(attrs={
                "class": "form-control",
            }),

        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['category'].queryset = Category.objects.exclude(root=None)
